<?php

namespace Faker\Test\Estonia;

use Faker\Estonia\Address;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    protected function setUp(): void
    {
        $faker = new Generator();
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    /**
     * Test the validity of postcode
     */
    public function testPostalCode()
    {
        $postcode = $this->_faker->postcode();

        $this->assertNotEmpty($postcode);

        $this->assertTrue(is_numeric($postcode));
        $this->assertEquals(5, strlen($postcode));
    }
}
