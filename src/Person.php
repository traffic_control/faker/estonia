<?php

namespace Faker\Estonia;

use Exception;

class Person extends \Faker\Provider\Person
{
    const CHECKSUM_DIGITS_SCALE_ONE = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1];
    const CHECKSUM_DIGITS_SCALE_TWO = [3, 4, 5, 6, 7, 8, 9, 1, 2, 3];

    protected static $genders = [self::GENDER_MALE, self::GENDER_FEMALE];

    /**
     * @see https://news.err.ee/114745/most-popular-baby-names-of-2014
     * @see https://www.stat.ee/public/apps/nimed/TOP
     */
    protected static $firstNameMale = [
        'Andrei', 'Aleksei', 'Andres', 'Alexander', 'Aivar', 'Aleksander', 'Artur', 'Andrus', 'Ants', 'Artjom', 'Anatoli',
        'Anton', 'Arvo', 'Aare', 'Ain', 'Aleksandr',
        'Dmitri', 'Daniil', 'Daniel',
        'Eduard', 'Erik', 'Enn',
        'Fjodorov',
        'Gennadi',
        'Heino', 'Henri', 'Hugo',
        'Igor', 'Indrek', 'Ivan', 'Ilja',
        'Jüri', 'Jaan', 'Jevgeni', 'Jaanus', 'Janek', 'Jaak',
        'Kristjan', 'Kalev', 'Karl', 'Kalle', 'Kaido', 'Kevin', 'Konstantin', 'Kaspar', 'Kirill', 'Kristo', 'Kalju', 'Kristofer',
        'Lauri', 'Lembit', 'Laur',
        'Martin', 'Margus', 'Maksim', 'Marko', 'Mati', 'Meelis', 'Mihhail', 'Marek', 'Mihkel', 'Mart', 'Madis', 'Markus', 'Mark', 'Marten',
        'Nikolai', 'Nikita', 'Nikolay',
        'Oleg', 'Oliver', 'Oskar',
        'Peeter', 'Priit', 'Pavel',
        'Rein', 'Roman', 'Raivo', 'Rasmus', 'Raul', 'Robert', 'Riho', 'Robin', 'Romet',
        'Sergei', 'Sander', 'Sergey', 'Siim', 'Silver', 'Sebastian',
        'Toomas', 'Tarmo', 'Tõnu', 'Tiit', 'Tanel', 'Taavi', 'Toivo', 'Tõnis',
        'Urmas', 'Ülo',
        'Vladimir', 'Viktor', 'Valeri', 'Vello', 'Vadim', 'Vitali', 'Vladislav', 'Vjatšeslav', 'Victor',
    ];

    /**
     * @see https://news.err.ee/114745/most-popular-baby-names-of-2014
     * @see https://www.stat.ee/public/apps/nimed/TOP
     */
    protected static $firstNameFemale = [
        'Aino', 'Aleksandra', 'Alisa', 'Anastasia', 'Anna', 'Anne', 'Anneli', 'Anu', 'Arina', 'Annika', 'Anastassia', 'Alla',
        'Aili', 'Alina', 'Aime', 'Antonina',
        'Darja', 'Diana',
        'Elena', 'Eliise', 'Elisabeth', 'Emma', 'Ene', 'Eve', 'Eha', 'Evi',
        'Galina',
        'Hanna', 'Helen', 'Heli', 'Helle', 'Helgi',
        'Irina', 'Inna', 'Ingrid',
        'Jekaterina', 'Jelena', 'Julia', 'Jana',
        'Kadri', 'Katrin', 'Kristi', 'Kristiina', 'Kristina', 'Karin', 'Kersti', 'Kristel', 'Kaja', 'Külli', 'Kätlin', 'Krista',
        'Laura', 'Lenna', 'Liisa', 'Linda', 'Lisandra', 'Ljubov', 'Ljudmila', 'Liina', 'Ljudmilla', 'Larissa', 'Liis', 'Lea', 'Laine', 'Liudmila',
        'Maie', 'Malle', 'Mare', 'Maria', 'Marina', 'Marleen', 'Marta', 'Merike', 'Mia', 'Milana', 'Mirtel', 'Marika', 'Merle',
        'Margit', 'Milvi', 'Maire', 'Margarita', 'Mari', 'Maarja',
        'Natalia', 'Niina', 'Nora', 'Natalja', 'Nadežda', 'Nina',
        'Olga', 'Oksana',
        'Piret', 'Polina', 'Pille',
        'Reet', 'Riina',
        'Sandra', 'Sirje', 'Sofia', 'Svetlana', 'Silvi',
        'Tamara', 'Tatiana', 'Tiina', 'Tiiu', 'Triin', 'Tatjana', 'Tiia',
        'Ülle', 'Urve',
        'Valentina', 'Viktoria', 'Veera', 'Veronika', 'Vaike',
        'Zinaida',
    ];

    /**
     * @see https://en.wikipedia.org/wiki/Category:Estonian-language_surnames
     * @see https://www.stat.ee/public/apps/nimed/pere/TOP
     */
    protected static $lastName = [
        'Aleksejev', 'Andrejev', 'Allik', 'Aas', 'Aleksandrov', 'Aare', 'Aarma', 'Aas', 'Aasmäe', 'Aav', 'Aavik', 'Allik', 'Alver',
        'Andrejeva', 'Aleksejeva', 'Aleksandrova', 'Allik', 'Aas',
        'Bogdanova', 'Bogdanov',
        'Eenpalu', 'Eskola',
        'Fjodorov', 'Fjodorova',
        'Grigorjev', 'Grigorjeva',
        'Hunt', 'Hein', 'Härma',
        'Ivanov', 'Ilves', 'Ivanova',
        'Jõgi', 'Jakobson', 'Jakovlev', 'Jürgenson', 'Jegorov', 'Järv', 'Johanson', 'Jakobson', 'Jänes', 'Järve', 'Järvis',
        'Jõgi', 'Jõgi', 'Johanson', 'Jürgenson', 'Jakovleva', 'Jegorova', 'Jakobson',
        'Kuzmina', 'Kalda', 'Kozlova', 'Kruus', 'Kask', 'Kukk', 'Koppel', 'Kaasik', 'Kuusk', 'Karu', 'Kütt', 'Kallas',
        'Kivi', 'Kangur', 'Kuusik', 'Kõiv', 'Kozlov', 'Kull', 'Kuzmin', 'Kaaleste', 'Käbin', 'Kaljulaid',
        'Kaljurand', 'Kallaste', 'Kangro', 'Kapp', 'Kärner', 'Käsper', 'Kass', 'Keres', 'Keskküla',
        'Kesküla', 'Kikkas', 'Kingsepp', 'Kirs', 'Kirsipuu', 'Klavan', 'Kokk', 'Kontaveit', 'Korjus', 'Kotkas',
        'Kreek', 'Kross', 'Kruus', 'Kuznetsov', 'Kuznetsova', 'Kuznetsova',
        'Luik', 'Lepik', 'Lepp', 'Lõhmus', 'Liiv', 'Laur', 'Leppik', 'Lebedev', 'Laas', 'Laar', 'Laht', 'Lass', 'Laurits', 'Lemsalu',
        'Lepik', 'Lepmets', 'Levandi', 'Lill', 'Lindmaa', 'Linna', 'Lipp', 'Lippmaa', 'Lõhmus', 'Loo', 'Lõoke',
        'Luts', 'Lepik', 'Lõhmus', 'Lebedeva',
        'Männik', 'Mänd', 'Mitt', 'Makarova', 'Mägi', 'Mets', 'Mihhailov', 'Mölder', 'Morozov', 'Mitt', 'Männik', 'Mõttus', 'Mänd', 'Makarov',
        'Mägi', 'Mälk', 'Mänd', 'Männik', 'Margiste', 'Mark', 'Masing', 'Mets', 'Mihhailov', 'Mihhailova', 'Mölder', 'Must', 'Mägi', 'Mets',
        'Mihhailova', 'Mölder', 'Morozova',
        'Nikolajev', 'Nõmm', 'Nikitin', 'Novikov', 'Nõmmik', 'Nurme', 'Nurmsalu', 'Nõmm', 'Nikitina', 'Nikolajeva',
        'Orlova', 'Orav', 'Oja', 'Ots', 'Orav', 'Orlov', 'Olesk', 'Öpik',
        'Petrov', 'Pärn', 'Põder', 'Pavlov', 'Popov', 'Peterson', 'Puusepp', 'Paju', 'Põld', 'Pukk', 'Paas', 'Palm', 'Pääsuke', 'Padar',
        'Pavlova', 'Peebo', 'Peetre', 'Peterson', 'Petrov', 'Petrova', 'Pihlak', 'Piho', 'Piip', 'Popov', 'Popova',
        'Poska', 'Puhvel', 'Pütsep', 'Puusepp', 'Petrova', 'Peterson', 'Popova', 'Puusepp', 'Paas', 'Paju', 'Pukk',
        'Parts', 'Palm',
        'Romanova', 'Rand', 'Roos', 'Rebane', 'Raudsepp', 'Raud', 'Roos', 'Rätsep', 'Raag', 'Raudsepp', 'Reek',
        'Reinsalu', 'Rooba', 'Roolaid', 'Rootare', 'Rummo', 'Rüütel', 'Rüütli',
        'Semjonov', 'Sokolov', 'Sild', 'Sarapuu', 'Saks', 'Saar', 'Salumäe', 'Sepp',
        'Sibul', 'Siimar', 'Simm', 'Sirel', 'Sisask', 'Smirnov', 'Smirnova', 'Sokk', 'Soosaar', 'Stepanov', 'Stepanova', 'Susi',
        'Sokolova', 'Semjonova',
        'Tamme', 'Tomson', 'Tamm', 'Teder', 'Toom', 'Talts', 'Tarvas', 'Toome', 'Toots', 'Teder',
        'Uibo', 'Uibo',
        'Vassiljev', 'Vaher', 'Volkov', 'Valk', 'Vaher', 'Vahtra', 'Vaino', 'Vainola', 'Välbe', 'Valdma', 'Väljas', 'Valk', 'Vassiljev',
        'Vassiljeva', 'Vesik', 'Veski', 'Viiding', 'Vitsut', 'Võigemast', 'Volkov', 'Volkova', 'Võsu', 'Vassiljeva', 'Vaher', 'Volkova',
    ];

    /**
     * National Personal Identity number (isikukood)
     *
     * @see https://en.wikipedia.org/wiki/National_identification_number#Estonia
     * @see https://et.wikipedia.org/wiki/Isikukood
     *
     * @param string    $gender       [male|female]
     * @param \DateTime $birthdate
     *
     * @return string on format XXXXXXXXXXX
     */
    public function personalIdentityNumber($gender = null, \DateTime $birthdate = null)
    {
        if (!$gender) {
            $gender = static::randomElement(static::$genders);
        }

        if (!$birthdate) {
            $birthdate = \Faker\Provider\DateTime::dateTimeThisCentury();
        }

        $genderNumber = ($gender == self::GENDER_MALE) ? 1 : 0;
        $firstNumber = (int) floor($birthdate->format('Y') / 100) * 2 - 34 - $genderNumber;

        $datePart = $birthdate->format('ymd');
        $randomDigits = static::numerify('###');
        $partOfPerosnalCode = $firstNumber . $datePart . $randomDigits;

        $sum = self::generateChecksum($partOfPerosnalCode);

        return $partOfPerosnalCode . $sum;
    }

    /**
     * Generates checksum from partial personal ID
     *
     * @param string $partialPersonalId
     *
     * @return int
     */
    private static function generateChecksum($partialPersonalId)
    {
        $checksum = self::calculateChecksum($partialPersonalId, self::CHECKSUM_DIGITS_SCALE_ONE);
        if ($checksum < 10) {
            return $checksum;
        }

        $checksum = self::calculateChecksum($partialPersonalId, self::CHECKSUM_DIGITS_SCALE_TWO);
        if ($checksum < 10) {
            return $checksum;
        }

        if ($checksum == 10) {
            return 0;
        }

        throw new Exception('Failed to generate checksum');
    }

    /**
     * @param string $personalId
     * @param array $scale
     *
     * @return int
     */
    private static function calculateChecksum($personalId, $scale)
    {
        $checksum = 0;
        foreach (str_split(substr($personalId, 0, 10)) as $index => $digit) {
            $checksum += $scale[$index] * $digit;
        }
        return $checksum % 11;
    }
}
