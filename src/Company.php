<?php

namespace Faker\Estonia;

use Exception;
use Faker\Extension\Extension;

class Company extends \Faker\Provider\Company implements Extension
{
    const CHECKSUM_DIGITS_SCALE_ONE = [1, 2, 3, 4, 5, 6, 7];
    const CHECKSUM_DIGITS_SCALE_TWO = [3, 4, 5, 6, 7, 8, 9];

    /**
     * Genarates an eight digit company registry code (registrikood).
     * First seven digits of the registry code are randomly genrated and the last digit is
     * generated from the first seven digits of the registry code using the 'Modulus 11' method.
     *
     * @see https://github.com/raigu/company-registry-code-validation
     *
     * @return string on format XXXXXXXX
     */
    public function companyRegistryCode()
    {
        $registryCodePartial = static::numerify('#######');

        $sum = self::generateChecksum($registryCodePartial);

        return $registryCodePartial . $sum;
    }

    /**
     * Generates checksum from partial registry code
     *
     * @param string $partialregistryCode
     *
     * @return int
     */
    private static function generateChecksum($partialregistryCode)
    {
        $checksum = self::calculateChecksum($partialregistryCode, self::CHECKSUM_DIGITS_SCALE_ONE);
        if ($checksum < 10) {
            return $checksum;
        }

        $checksum = self::calculateChecksum($partialregistryCode, self::CHECKSUM_DIGITS_SCALE_TWO);
        if ($checksum < 10) {
            return $checksum;
        }

        if ($checksum == 10) {
            return 0;
        }

        throw new Exception('Failed to generate checksum');
    }

    /**
     * @param string $partialregistryCode
     * @param array $scale
     *
     * @return int
     */
    private static function calculateChecksum($partialregistryCode, $scale)
    {
        $checksum = 0;
        foreach (str_split(substr($partialregistryCode, 0, 7)) as $index => $digit) {
            $checksum += $scale[$index] * $digit;
        }
        return $checksum % 11;
    }
}
