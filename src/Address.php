<?php

namespace Faker\Estonia;

use Faker\Extension\Extension;

class Address extends \Faker\Provider\Address implements Extension
{
    protected static $buildingNumber = ['##?', '%#', '##-?'];
    protected static $postcode = ['#####'];

    /**
     * @see https://et.wikipedia.org/wiki/Riikide_loend
     */
    protected static $country = [
        'Afganistan', 'Albaania', 'Alžeeria', 'Ameerika Ühendriigid', 'Andorra', 'Angola', 'Antigua ja Barbuda',
        'Araabia Ühendemiraadid', 'Argentina', 'Armeenia', 'Aserbaidžaan', 'Austraalia', 'Austria', 'Bahama',
        'Bahrein', 'Bangladesh', 'Barbados', 'Belau', 'Belgia', 'Belize', 'Benin',
        'Bhutan', 'Boliivia', 'Bosnia ja Hertsegoviina', 'Botswana', 'Brasiilia', 'Brunei', 'Bulgaaria',
        'Burkina Faso', 'Burundi', 'Cabo Verde', 'Colombia', 'Costa Rica', 'Côte d' . "'" . 'Ivoire', 'Djibouti',
        'Dominica', 'Dominikaani Vabariik', 'Ecuador', 'Eesti', 'Egiptus', 'Ekvatoriaal-Guinea', 'El Salvador',
        'Eritrea', 'eSwatini', 'Etioopia', 'Fidži', 'Filipiinid', 'Gabon', 'Gambia',
        'Ghana', 'Grenada', 'Gruusia', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana',
        'Haiti', 'Hiina RV', 'Hispaania', 'Holland', 'Honduras', 'Horvaatia', 'Ida-Timor',
        'Iirimaa', 'Iisrael', 'India', 'Indoneesia', 'Iraak', 'Iraan', 'Island',
        'Itaalia', 'Jaapan', 'Jamaica', 'Jeemen', 'Jordaania', 'Kambodža', 'Kamerun',
        'Kanada', 'Kasahstan', 'Katar', 'Keenia', 'Kesk-Aafrika Vabariik', 'Kiribati', 'Komoorid',
        'Kongo DV', 'Kongo Vabariik', 'Kosovo', 'Kreeka', 'Kuuba', 'Kuveit', 'Kõrgõzstan',
        'Küpros', 'Laos', 'Leedu', 'Lesotho', 'Libeeria', 'Liechtenstein', 'Liibanon',
        'Liibüa', 'Luksemburg', 'Lõuna-Aafrika Vabariik', 'Lõuna-Korea', 'Lõuna-Sudaan', 'Läti', 'Madagaskar',
        'Malaisia', 'Malawi', 'Maldiivid', 'Mali', 'Malta', 'Maroko', 'Marshalli Saared',
        'Mauritaania', 'Mauritius', 'Mehhiko', 'Mikroneesia Liiduriigid', 'Moldova', 'Monaco', 'Mongoolia',
        'Montenegro', 'Mosambiik', 'Myanmar (Birma)', 'Namiibia', 'Nauru', 'Nepal', 'Nicaragua',
        'Nigeeria', 'Niger', 'Norra', 'Omaan', 'Paapua Uus-Guinea', 'Pakistan', 'Palestiina Riik',
        'Panama', 'Paraguay', 'Peruu', 'Poola', 'Portugal', 'Prantsusmaa', 'Põhja-Korea',
        'Põhja-Makedoonia', 'Rootsi', 'Rumeenia', 'Rwanda', 'Saalomoni Saared', 'Saint Kitts ja Nevis', 'Saint Lucia',
        'Saint Vincent ja Grenadiinid', 'Saksamaa', 'Sambia', 'Samoa', 'San Marino', 'São Tomé ja Príncipe', 'Saudi Araabia',
        'Seišellid', 'Senegal', 'Serbia', 'Sierra Leone', 'Singapur', 'Slovakkia', 'Sloveenia',
        'Somaalia', 'Soome', 'Sri Lanka', 'Sudaan', 'Suriname', 'Suurbritannia', 'Süüria',
        'Šveits', 'Zimbabwe', 'Taani', 'Tadžikistan', 'Tai', 'Taiwan (Hiina Vabariik)', 'Tansaania',
        'Togo', 'Tonga', 'Trinidad ja Tobago', 'Tšaad', 'Tšehhi', 'Tšiili', 'Tuneesia',
        'Tuvalu', 'Türgi', 'Türkmenistan', 'Uganda', 'Ukraina', 'Ungari', 'Uruguay',
        'Usbekistan', 'Uus-Meremaa', 'Valgevene', 'Vanuatu', 'Vatikan', 'Venemaa', 'Venezuela',
        'Vietnam',
    ];

    protected static $city = [
        'Tallinn', 'Tartu', 'Narva', 'Pärnu', 'Kohtla-Järve', 'Viljandi', 'Maardu',
        'Rakvere', 'Kuressaare', 'Sillamäe', 'Valga', 'Võru', 'Jõhvi', 'Keila',
        'Haapsalu', 'Paide', 'Saue', 'Elva', 'Tapa', 'Põlva', 'Türi',
        'Rapla', 'Jõgeva', 'Kiviõli', 'Põltsamaa', 'Sindi', 'Paldiski', 'Kärdla',
        'Kunda', 'Tõrva', 'Narva-Jõesuu', 'Kehra', 'Loksa', 'Otepää', 'Räpina',
        'Tamsalu', 'Kilingi-Nõmme', 'Karksi-Nuia', 'Võhma', 'Antsla', 'Lihula', 'Mustvee',
        'Suure-Jaani', 'Abja-Paluoja', 'Püssi', 'Mõisaküla', 'Kallaste',
    ];

    protected static $street = [
        'Aarde', 'Aasa', 'Aate', 'Abaja', 'Abaja', 'Abara', 'Amandus', 'Admiraliteedi', 'Adra',
        'Aedvere', 'Aedvilja', 'Aegna', 'Ahingu', 'Ahju', 'Ahtri', 'Ahvena', 'Aia', 'Aiandi',
        'Aianduse', 'Aia', 'Aida', 'Aisa', 'Akadeemia', 'Alajaama', 'Alasi', 'Alberti', 'Alemaa',
        'Alendri', 'Alevi', 'Algi', 'Algi', 'August', 'Allika', 'Allika', 'Alliksoo', 'Alliksoo',
        'Allveelaeva', 'Alvari', 'Amburi', 'Andrekse', 'Angerja', 'Angerpisti', 'Angervaksa', 'Ankru', 'Anne',
        'Anni', 'Ao', 'Apteegi', 'Arbu', 'Armatuuri', 'Arnika', 'Artelli', 'Aru', 'Arukaskede',
        'Asfaldi', 'Elisabeth', 'Astangu', 'Astla', 'Astri', 'Asula', 'Asula', 'Asunduse', 'Auli',
        'Auna', 'Auru', 'Auru', 'Baikovi', 'Bensiini', 'Betooni', 'Betooni', 'Eduard',
        'Bremeni', 'Börsi', 'Drevingi', 'Dunkri', 'Edela', 'Edu', 'Eerikneeme',
        'Eevardi', 'Eha', 'Ehitajate', 'Ehte', 'Elektri', 'Elektroni', 'Endla', 'Enela', 'Energia',
        'Erbe', 'Erika', 'Esku', 'Estonia', 'Friedrich', 'Falgi', 'Falkpargi', 'Filmi',
        'Filtri', 'Forelli', 'Gaasi', 'Gildi', 'Nikolai', 'Nikolai', 'Gonsiori', 'Graniidi',
        'Grigori', 'Gümnaasiumi', 'Haabersti', 'Haava', 'Haava', 'Haaviku', 'Haaviku', 'Hagudi',
        'Haigru', 'Haki', 'Haldja', 'Haljas', 'Halla', 'Hallikivi', 'Hallivanamehe', 'Halsi', 'Halu',
        'Hane', 'Hange', 'Hanijala', 'Hansu', 'Hao', 'Haraka', 'Hargi', 'Hariduse', 'Harju',
        'Harksaba', 'Harku', 'Harkumetsa', 'Hauka', 'Havi', 'Heeringa', 'Heina', 'Heinamaa', 'Heinavälja',
        'Heki', 'Helbe', 'Heli', 'Helme', 'Helmiku', 'Helmiku', 'Herilase', 'Karl', 'Herne',
        'Hiidtamme', 'Hiie', 'Hiie', 'Hiiela', 'Hiiepuu', 'Hiiu', 'Hiiu-Maleva', 'Hiiumetsa', 'Hiiu-Suurtüki',
        'Hipodroomi', 'Hirve', 'Hirve', 'Hobujaama', 'Hoburaua', 'Hobusepea', 'Hollandi', 'Hollandi', 'Hommiku',
        'Hoo', 'Hooldekodu', 'Hospitali', 'Humala', 'Hundipea', 'Hunditubaka', 'Hõbeda', 'Hõbekuuse', 'Hõberebase',
        'Hõimu', 'Häilu', 'Hälli', 'Hälli', 'Hämar', 'Hämariku', 'Härgmäe', 'Härjapea', 'Miina',
        'Härmatise', 'Hüübi', 'Ida', 'Idakaare', 'Idakaare', 'Iirise', 'Ilmarise', 'Ilo',
        'Ilvese', 'Imanta', 'Inseneri', 'Invaliidi', 'Iru', 'Irusilla', 'Islandi', 'Israeli', 'Iva',
        'Jaagu', 'Jaama', 'Jaani', 'Jaaniku', 'Jaanilille', 'Jaanilille', 'Jahe', 'Jahimehe',
        'Jahimehe', 'Jahu', 'Jakobi', 'Carl', 'Jalaka', 'Jalami', 'Jalgpalli', 'Jalgpalli', 'Johann',
        'Jasmiini', 'Joa', 'Joone', 'Joostimäe', 'Jugapuu', 'Jugapuu', 'Juhkentali', 'Juhtme', 'Jumika',
        'Jussikalda', 'Juure', 'Juurdeveo', 'Jõe', 'Jõekalda', 'Jõeküla', 'Jõemäe', 'Jõeoti', 'Jõesaare',
        'Jõesuu', 'Jõhvika', 'Järve', 'Järveaasa', 'Järvekalda', 'Järveotsa', 'Järvevana', 'Jäägri', 'Jääraku',
        'Jääraku', 'Jüri', 'Jürilille', 'Kaabli', 'Kaare', 'Kaare', 'Kaarla', 'Kaarlepere',
        'Kaarli', 'Kaarna', 'Kaaruti', 'Kaasani', 'Kaasiku', 'Kabli', 'Kadaka', 'Kadaka', 'Kadri',
        'Kaera', 'Kaeravälja', 'Kaevu', 'Kaevuri', 'Kagu', 'Kaheküla', 'Kahlu', 'Kahu', 'Kahva',
        'Kai', 'Kailu', 'Kaisla', 'Kaitse', 'Kaja', 'Kajaka', 'Kakumäe', 'Kaladi', 'Kalamehe',
        'Kalaranna', 'Kalasadama', 'Kalavälja', 'Kalda', 'Kaldapealse', 'Kalevala', 'Kalevi', 'Kalevipoja', 'Kalevipoja',
        'Kaljase', 'Kalju', 'Kalju', 'Kallaku', 'Kallaste', 'Kalma', 'Kalmistu', 'Kalmuse', 'Kaluri',
        'Kammelja', 'Kampri', 'Kana', 'Kanali', 'Kanarbiku', 'Kandle', 'Kanepi', 'Kangru', 'Kannikese',
        'Kannustiiva', 'Kantsi', 'Kanuti', 'Artur', 'Johannes', 'Karamelli', 'Kari', 'Karjamaa', 'Karjamaa',
        'Karjatse', 'Karjavälja', 'Karnapi', 'Karsti', 'Karu', 'Karukella', 'Karusambla', 'Karuse', 'Karuslase',
        'Karusmarja', 'Kasarmu', 'Kase', 'Kaskede', 'Kassi', 'Kassikäpa', 'Kastani', 'Kaste', 'Kasteheina',
        'Kastevarre', 'Kastiku', 'Kasvu', 'Katariina', 'Katla', 'Katleri', 'Katusepapi', 'Kauba', 'Kaubamaja',
        'Kauge', 'Kauka', 'Kauna', 'Kaunis', 'Kaupmehe', 'Kauri', 'Keava', 'Kedriku', 'Keemia',
        'Keeru', 'Keevise', 'Keldrimäe', 'Kelluka', 'Kentmanni', 'Kentmanni', 'Paul', 'Kesa', 'Kesalille',
        'Keskküla', 'Kesk-Ameerika', 'Kesk-Kalamaja', 'Kesk-Liiva', 'Kesk-Luha', 'Kesk-Kompassi', 'Kesk-Sõjamäe', 'Kesktee', 'Keskuse',
        'Kessi', 'Ketraja', 'Ketta', 'Kevade', 'Kibuvitsa', 'Kihnu', 'Kiige', 'Kiikri', 'Kiili',
        'Kiini', 'Kiire', 'Kiisa', 'Kiive', 'Kikkapuu', 'Kilbi', 'Kilde', 'Killustiku', 'Kilu',
        'Kinga', 'Kirde', 'Kiriku', 'Kiriku', 'Kiriku', 'Kirilase', 'Kirsi', 'Kitsarööpa', 'Kitsas',
        'Kitseküla', 'August', 'Kiuru', 'Kivi', 'Kiviaia', 'Kivikülvi', 'Kivila', 'Kivimurru', 'Kivimäe',
        'Kivinuka', 'Kivipere', 'Kiviranna', 'Kiviriku', 'Kiviräha', 'Kivisilla', 'Klaasi', 'Kloostri', 'Kloostrimetsa',
        'Kodu', 'Koge', 'Kohila', 'Kohtu', 'Koidiku', 'Koidu', 'Lydia', 'Kolde', 'Koldrohu',
        'Kollane', 'Komandandi', 'Komeedi', 'Kompassi', 'Kontpuu', 'Koogu', 'Kooli', 'Koolme', 'Jaan',
        'Koovitaja', 'Kopli', 'Kopliranna', 'Korese', 'Korgi', 'Kose', 'Kose', 'Kosemetsa', 'Koskla',
        'Kotermaa', 'Kotka', 'Kotkapoja', 'Kotlepi', 'Kotzebue', 'Kraavi', 'Kreegi', 'Kreegi', 'Kressi',
        'Friedrich', 'Kriidi', 'Kristeni', 'Kristiina', 'Krookuse', 'Krooni', 'Kruusa', 'Kruusaranna', 'Krüüsli',
        'Kubu', 'Kudu', 'Kuhja', 'Friedrich', 'Kuiv', 'Kuke', 'Kukermiidi', 'Kuklase', 'Kukulinnu',
        'Kuldnoka', 'Kuldtiiva', 'Kuljuse', 'Kullamaa', 'Kullassepa', 'Kullerkupu', 'Kullese', 'Kulli', 'Kumalase',
        'Kume', 'Kummeli', 'Juhan', 'Juhan', 'Kungla', 'Kuninga', 'Kupra', 'Kura', 'Kure',
        'Kurekatla', 'Kuremarja', 'Kurepõllu', 'Kurereha', 'Kurikneeme', 'Kuristiku', 'Kurmu', 'Kurni', 'Kurni',
        'Kursi', 'Kuru', 'Kuslapuu', 'Kuukivi', 'Kuukressi', 'Kuuli', 'Kuunari', 'Kuuse', 'Kuusenõmme',
        'Kuusiku', 'Kvartsi', 'Kvartsliiva', 'Kõdra', 'Kõivu', 'Kõivu', 'Kõla', 'Kõlviku', 'Kõlviku',
        'Kõnnu', 'Kõrge', 'Kõrgepinge', 'Kõrkja', 'Kõrre', 'Kõue', 'Kõver', 'Käba', 'Käbi',
        'Käbliku', 'Käia', 'Kännu', 'Kännu', 'Käo', 'Käo', 'Käokannu', 'Käokeele', 'Käokäpa',
        'Käolina', 'Käopoja', 'Kristjan', 'Kärbi', 'Kärestiku', 'Kärje', 'Kärneri', 'Käru', 'Kätki',
        'Käänu', 'Köie', 'Johann', 'Köömne', 'Külaniidu', 'Külma', 'Külmallika', 'Külmallika', 'Külvi',
        'Künka', 'Künkamaa', 'Künnapuu', 'Künni', 'Küti', 'Küüvitsa', 'Laagna', 'Laane',
        'Laanelille', 'Laaniku', 'Laboratooriumi', 'Laeva', 'Laeva', 'Laevastiku', 'Lagedi', 'Lageloo', 'Lagle',
        'Lahe', 'Lahepea', 'Lahesuu', 'Lai', 'Laiaküla', 'Ants', 'Laine', 'Laki', 'Lambi',
        'Lammi', 'Landi', 'Laose', 'Lasnamäe', 'Laste', 'Lastekodu', 'Latika', 'Laugu', 'Lauka',
        'Lauliku', 'Laulu', 'Laulupeo', 'Lauri', 'Laurimaa', 'Lauripere', 'Ants', 'Lavamaa', 'Lee',
        'Leediku', 'Leedri', 'Leegi', 'Leesika', 'Leete', 'Leevikese', 'Lehe', 'Lehiku', 'Lehise',
        'Lehiste', 'Lehola', 'Lehtmäe', 'Lehtpuu', 'Lehtri', 'Leigeri', 'Leina', 'Leiva', 'Lelle',
        'Lembitu', 'Lemle', 'Lemmiku', 'Lemmiku', 'Lennujaama', 'Lennuki', 'Lennusadama', 'Lennuse', 'Lepa',
        'Lepa', 'Lepatriinu', 'Lepiku', 'Lesta', 'Lible', 'Liblika', 'Liikuri', 'Liilia', 'Liimi',
        'Liinivahe', 'Liipri', 'Liiva', 'Liivaaugu', 'Liivalaia', 'Liivalao', 'Liivaluite', 'Liivamadala', 'Liivametsa',
        'Liivamäe', 'Liivaoja', 'Liivaranna', 'Liiviku', 'Lille', 'Lilleherne', 'Lillevälja', 'Lina', 'Linda',
        'Lindakivi', 'Linnamäe', 'Linnu', 'Linnuse', 'Lobjaka', 'Lodjapuu', 'Lodumetsa', 'Logi', 'Lohu',
        'Loigu', 'Loite', 'Loitsu', 'Loo', 'Loode', 'Loode', 'Looga', 'Looklev', 'Loometsa',
        'Loomuse', 'Loopealse', 'Lootsi', 'Lootuse', 'Lootuse', 'Lossi', 'Lossi', 'Lubja', 'Luha',
        'Luige', 'Luise', 'Luisu', 'Luite', 'Luite', 'Luku', 'Lume', 'Lumikellukese', 'Lumiku',
        'Lummu', 'Luste', 'Lutheri', 'Lutheri', 'Lõhe', 'Lõhmuse', 'Lõhmuse', 'Lõikuse', 'Lõime',
        'Lõimise', 'Lõkke', 'Lõo', 'Lõokese', 'Lõosilma', 'Lõuka', 'Lõuna', 'Lõvi', 'Lõõtsa',
        'Läike', 'Lätte', 'Lääne', 'Läänekaare', 'Läänemere', 'Läätspuu', 'Lühike', 'Lühike', 'Lükati',
        'Lüli', 'Lüüsi', 'Maakri', 'Maarjaheina', 'Maarjamäe', 'Maasika', 'Madala', 'Madalmaa',
        'Madara', 'Madli', 'Magasini', 'Magdaleena', 'Mahla', 'Mahtra', 'Mai', 'Mai', 'Maikellukese',
        'Mailase', 'Maimu', 'Maisi', 'Majaka', 'Majaka', 'Male', 'Maleva', 'Maleva', 'Malmi',
        'Maneeži', 'Manufaktuuri', 'Marana', 'Marati', 'Mardi', 'Mardipere', 'Marja', 'Marjamaa', 'Märjamaa',
        'Marsi', 'Marta', 'Martsa', 'Masina', 'Masti', 'Matka', 'Meeliku', 'Meeruse', 'Meeta',
        'Mehaanika', 'Mehaanika', 'Meika', 'Meistri', 'Meleka', 'Mere', 'Merelahe', 'Meremehe', 'Merihärja',
        'Merekalju', 'Merikanni', 'Merimetsa', 'Merinõela', 'Merirahu', 'Meriski', 'Merivälja', 'Mesika', 'Mesika',
        'Mesila', 'Metalli', 'Metsa', 'Metsa', 'Metsakooli', 'Metsakooli', 'Mait', 'Metsavahi', 'Metsavahi',
        'Metsaveere', 'Metsise', 'Metsniku', 'Mihkli', 'Miinisadama', 'Miku', 'Mineraali', 'Mingi', 'Mirta',
        'Moonalao', 'Mooni', 'Moora', 'Moora', 'Moora', 'Mootori', 'Moskva', 'Mugula', 'Muhu',
        'Muldvalge', 'Mulla', 'Mulla', 'Mullamaa', 'Munga', 'Muraka', 'Mureli', 'Muru', 'Mustakivi',
        'Mustamäe', 'Mustasõstra', 'Mustika', 'Mustjuure', 'Mustjõe', 'Mustjõe', 'Muti', 'Muuga', 'Muuluka',
        'Mõigu', 'Mõisa', 'Mõisapõllu', 'Mõrra', 'Mõtuse', 'Mõõna', 'Mäe', 'Mäealuse', 'Mäekalda',
        'Mäekõrtsi', 'Mäeküla', 'Mäepealse', 'Mäepere', 'Mäevälja', 'Mägra', 'Mähe', 'Mähe', 'Mähe-Kaasiku',
        'Jakob', 'Mängu', 'Männi', 'Männiku', 'Männiliiva', 'Männimetsa', 'Männimetsa', 'Mänsaku', 'Märtsikellukese',
        'Möldre', 'Mündi', 'Müta', 'Müürivahe', 'Naadi', 'Naaritsa', 'Nabra', 'Naeri',
        'Nafta', 'Naistepuna', 'Narva', 'Nata', 'Neeme', 'Neeme', 'Neemiku', 'Neiuvaiba', 'Nelgi',
        'Nepi', 'Nigli', 'Niguliste', 'Niidi', 'Niidi', 'Niidu', 'Niine', 'Niine', 'Nirgi',
        'Nisu', 'Nooda', 'Noole', 'Noorkuu', 'Nugise', 'Nuia', 'Nulu', 'Nunne', 'Nurklik',
        'Nurme', 'Nurmenuku', 'Nurmiku', 'Nurmiku', 'Nõelasilma', 'Nõeliku', 'Nõgikikka', 'Nõlva', 'Nõmme',
        'Nõmme', 'Nõmme-Kase', 'Nõo', 'Nõva', 'Näituse', 'Näsiniine', 'Oa', 'Oblika',
        'Oda', 'Odra', 'Ogaliku', 'Oja', 'Ojakääru', 'Ojaveere', 'Okka', 'Oksa', 'Olevi',
        'Olevi', 'Olevimägi', 'Oleviste', 'Oomi', 'Orase', 'Orava', 'Oru', 'Osja', 'Osmussaare',
        'Oti', 'Georg', 'Otsatalu', 'Otse', 'Paabusilma', 'Paadi', 'Paagi', 'Paakspuu',
        'Paasiku', 'Paavli', 'Paberi', 'Padriku', 'Padu', 'Pae', 'Paekaare', 'Paekalda', 'Paekivi',
        'Paepargi', 'Paevälja', 'Pagari', 'Pagi', 'Paide', 'Paiste', 'Paisu', 'Paju', 'Pajude',
        'Pajulille', 'Pajustiku', 'Pakase', 'Palderjani', 'Paldiski', 'Paljandi', 'Paljassaare', 'Paljassaare', 'Pallasti',
        'Palli', 'Palu', 'Paluka', 'Palusambla', 'Paneeli', 'Panga', 'Papli', 'Paplite', 'Parda',
        'Pardi', 'Pardiloigu', 'Pargi', 'Parmu', 'Parve', 'Patriarh', 'Pebre', 'Pedaja', 'Peetri',
        'Pelguranna', 'Pendi', 'Pesa', 'Peterburi', 'Petrooleumi', 'Pidu', 'Pihlaka', 'Pihlamarja', 'Pihlametsa',
        'Piibelehe', 'Piibri', 'Piima', 'Piimamehe', 'Piipheina', 'Piiri', 'Piiritsa', 'Piiskopi', 'Pikakase',
        'Pikaliiva', 'Pikk', 'Pikk', 'Pikksilma', 'Pikri', 'Pikse', 'Piksepeni', 'Pille', 'Pilliroo',
        'Pilve', 'Pilvetee', 'Pilviku', 'Paul', 'Pinu', 'Pirita', 'Pirni', 'Plaasi', 'Planeedi',
        'Plangu', 'Plasti', 'Ploomi', 'Pohla', 'Pojengi', 'Polaari', 'Poldri', 'Poordi', 'Poru',
        'Jaan', 'Postitalu', 'Preesi', 'Prii', 'Prii', 'Priimula', 'Priimurru', 'Priisle', 'Printsu',
        'Pronksi', 'Pruuali', 'Puhangu', 'Puhke', 'Puhkekodu', 'Puhma', 'Puju', 'Puki', 'Pukspuu',
        'Punane', 'Pune', 'Punga', 'Puraviku', 'Purde', 'Purje', 'Puu', 'Puusepa', 'Puuvilja',
        'Puuvilla', 'Põdra', 'Põdrakanepi', 'Põdrasambla', 'Põhja', 'Põhjakaare', 'Põhjakants', 'Põlde', 'Põlde',
        'Põldma', 'Põldmarja', 'Põlendiku', 'Põllu', 'Põllu', 'Põllumäe', 'Põõsa', 'Päevakoera', 'Päevalille',
        'Pähkli', 'Päikese', 'Pärituule', 'Pärja', 'Jakob', 'Pärnade', 'Pärnamäe', 'Pärnaõie', 'Pärniku',
        'Pärnu', 'Pärnulõuka', 'Pääsküla', 'Pääsukese', 'Pääsusaba', 'Pääsusilma', 'Pöialpoisi', 'Pöörise', 'Pühavaimu',
        'Püssirohu', 'Püü', 'Püü', 'Raadiku', 'Raba', 'Rabaküla', 'Rabaveere', 'Raekoja',
        'Raekoja', 'Raevalla', 'Rahe', 'Rahu', 'Rahu', 'Rahukohtu', 'Rahumäe', 'Rahvakooli', 'Raiduri',
        'Raie', 'Raja', 'Rajametsa', 'Rajapere', 'Rakise', 'Raku', 'Randla', 'Randvere', 'Rangu',
        'Ranna', 'Rannamõisa', 'Rannamäe', 'Rannaniidu', 'Ranniku', 'Ranniku', 'Rao', 'Rapla', 'Rataskaevu',
        'Ratta', 'Raua', 'Raua', 'Kristjan', 'Rauakooli', 'Raudosja', 'Raudsüdame', 'Raudtee', 'Ravi',
        'Ravila', 'Rebase', 'Rebasesaba', 'Regati', 'Reha', 'Rehe', 'Rehe', 'Rehelepa', 'Rehelepa',
        'Reidi', 'Villem', 'Ado', 'Reisijate', 'Reketi', 'Remmelga', 'Reseeda', 'Retke', 'Riida',
        'Riisika', 'Ristaia', 'Risti', 'Ristiku', 'Ristiku', 'Rivi', 'Rocca', 'Roheline', 'Roheline',
        'Rohu', 'Rohula', 'Rohumaa', 'Ronga', 'Roo', 'Roolahe', 'Roopa', 'Roosi', 'Roosikrantsi',
        'Roostiku', 'Roseni', 'Rotermanni', 'Rukki', 'Rukkilille', 'Rulkoviuse', 'Rulli', 'Rumbi', 'Rummu',
        'Russovi', 'Rutu', 'Ruunaoja', 'Rõika', 'Rõugu', 'Rõõmu', 'Räga', 'Rähkloo', 'Rähni',
        'Räime', 'Räitsaka', 'Rändrahnu', 'Ränduri', 'Räni', 'Räniliiva', 'Rännaku', 'Rästa', 'Rästa',
        'Rätsepa', 'Rävala', 'Räägu', 'Ründi', 'Rünga', 'Rüütli', 'Saadu', 'Saagi',
        'Saali', 'Saani', 'Saare', 'Saaremaa', 'Saarepiiga', 'Saarepuu', 'Saariku', 'Saarma', 'Saarma',
        'Saarvahtra', 'Sadama', 'Saeveski', 'Sagari', 'Saha', 'Saha', 'Saiakang', 'Sakala', 'Saku',
        'Salme', 'Salu', 'Salve', 'Sambla', 'Sambliku', 'Sammu', 'Sanatooriumi', 'Sanglepa', 'Sarapiku',
        'Sarapuu', 'Sarra', 'Sarruse', 'Sarve', 'Saturni', 'Saue', 'Sauna', 'Saviliiva', 'Seebi',
        'Seedri', 'Seedri', 'Seemne', 'Seene', 'Seli', 'Selise', 'Selja', 'Seljaku', 'Seltersi',
        'Sepa', 'Sepapaja', 'Sepapere', 'Sepapoisi', 'Sepise', 'Septembri', 'Serva', 'Side', 'Siduri',
        'Sihi', 'Siia', 'Siidisaba', 'Siili', 'Siimeoni', 'Siire', 'Sikupilli', 'Sikuti', 'Silde',
        'Silgu', 'Silikaadi', 'Silikaltsiidi', 'Silla', 'Silluse', 'Silmiku', 'Silmu', 'Sinika', 'Siniladva',
        'Sinilille', 'Sinimäe', 'Sininuku', 'Sinirebase', 'Sinitiiva', 'Sipelga', 'Sirbi', 'Sireli', 'Sirge',
        'Sisaski', 'Sitsi', 'Juhan', 'Sompa', 'Soo', 'Soodi', 'Sookailu', 'Sookaskede', 'Soolahe',
        'Soone', 'Soone', 'Sooranna', 'Sootaga', 'Sooviku', 'Soovildiku', 'Soovõha', 'Spordi', 'Staadioni',
        'Staapli', 'Suislepa', 'Suitsu', 'Suitsu', 'Suklema', 'Sule', 'Sulevi', 'Sulevimägi', 'Sumba',
        'Supluse', 'Suru', 'Suur', 'Suur-Ameerika', 'Suurekivi', 'Suurevälja', 'Suurgildi', 'Suur-Karja', 'Suur-Karjamaa',
        'Suur-Kloostri', 'Suur-Kompassi', 'Suur-Laagri', 'Suur-Laevaehituse', 'Suur-Lasnamäe', 'Suur-Männiku', 'Suur-Paala', 'Suur-Patarei', 'Suur-Pääsukese',
        'Suur-Raevalla', 'Suur-Sõjamäe', 'Suur-Sõjamäe', 'Suur-Söörensi', 'Suur-Tartu', 'Suurtüki', 'Suusa', 'Suve', 'Suvila',
        'Sõbra', 'Sõbra', 'Sõjakooli', 'Sõle', 'Sõlme', 'Sõmera', 'Sõnajala', 'Sõpruse', 'Sõstra',
        'Sõstramäe', 'Sõudebaasi', 'Sõõru', 'Sädeme', 'Säina', 'Ernst', 'Särje', 'Särjesilma', 'Sääse',
        'Söe', 'Söödi', 'Peeter', 'Sügise', 'Sügislase', 'Süsta', 'Juhan', 'Taagi',
        'Taara', 'Taela', 'Taevakivi', 'Taevastiiva', 'Tagala', 'Tagamaa', 'Taime', 'Tala', 'Talli',
        'Tallinna', 'Taludevahe', 'Talve', 'Talviku', 'Tambeti', 'Tamme', 'Tammede', 'Tammepärja', 'Tammetõru',
        'Tammiku', 'Tanuma', 'Tapri', 'Tare', 'Tarja', 'Tarna', 'Tartu', 'Tasandi',
        'Tasuja', 'Tasuja', 'Tatari', 'Teaduspargi', 'Teatri', 'Tedre', 'Tedre', 'Tedrepere', 'Teelehe',
        'Teelise', 'Teemeistri', 'Tehnika', 'Teisepere', 'Teivi', 'Telliskivi', 'Tendri', 'Terase', 'Tervise',
        'Tihase', 'Tihniku', 'Tiigi', 'Tiiru', 'Tiiu', 'Tiiva', 'Tikutaja', 'Tildri', 'Tildri',
        'Timuti', 'Tina', 'Tindi', 'Tipi', 'Tirdi', 'Tirgu', 'Tiskrevälja', 'Rudolf', 'Tohu',
        'Tolli', 'Tolmuka', 'Tondi', 'Tondiraba', 'Tominga', 'Toodri', 'Tooma', 'Toomapere', 'Toome',
        'Toome', 'Toomiku', 'Toominga', 'Toom-Kooli', 'Toom-Kuninga', 'Toompea', 'Toompuiestee', 'Toom-Rüütli', 'Toonela',
        'Topi', 'Tormi', 'Torni', 'Tornimäe', 'Torupilli', 'Trahteri', 'Treiali', 'Trepi', 'Trummi',
        'Trummi', 'Truubi', 'Tselluloosi', 'Tuha', 'Tuhkpuu', 'Tuhkru', 'Tuiskliiva', 'Tuisu', 'Tuki',
        'Tulbi', 'Tulekivi', 'Tuleraua', 'Tuletorni', 'Tulika', 'Tulika', 'Tulimulla', 'Tuluste', 'Tulva',
        'Tungla', 'Tupsi', 'Turba', 'Tursa', 'Turu', 'Turu', 'Turvise', 'Tuudi', 'Tuukri',
        'Tuukri', 'Tuulemaa', 'Tuulemäe', 'Tuulenurga', 'Tuuleveski', 'Tuuliku', 'Tuvi', 'Tõivu', 'Tõkke',
        'Tõllu', 'Tõnismägi', 'Tõnu', 'Tõru', 'Tõrviku', 'Tõusu', 'Tähe', 'Tähesaju', 'Tähetorni',
        'Tähistaeva', 'Täpiku', 'Töökoja', 'Tööstuse', 'Türi', 'Konstantin', 'Tüve', 'Ubalehe',
        'Udu', 'Udeselja', 'Ugala', 'Uime', 'Ujuki', 'Uku', 'Umboja', 'Uneliblika', 'Unistuse',
        'Unna', 'Urva', 'Ussilaka', 'Ussimäe', 'Uue', 'Uuepere', 'Uueristi', 'Uus', 'Uus-Kalamaja',
        'Uuslinna', 'Uus-Hollandi', 'Uus-Maleva', 'Uus-Sadama', 'Uus-Tatari', 'Uus-Tui', 'Uus-Volta', 'Vaablase',
        'Vaagi', 'Vaalu', 'Vaari', 'Vaarika', 'Vaate', 'Vaate', 'Vabaduse', 'Vabaduse', 'Vabarna',
        'Vabaõhukooli', 'Vabe', 'Vabriku', 'Vaestepatuste', 'Vahe', 'Vahepere', 'Vahtra', 'Vahtramäe', 'Vahtriku',
        'Vahulille', 'Vahuri', 'Vaigu', 'Vaikne', 'Vaimu', 'Vainu', 'Vainutalu', 'Vainutalu', 'Vaksiku',
        'Valdeku', 'Valge', 'Valgevase', 'Valguse', 'Valguta', 'Valli', 'Valukoja', 'Valve', 'Vambola',
        'Vana-Kalamaja', 'Vana-Keldrimäe', 'Vanakuu', 'Vana-Kuuli', 'Vana-Liivamäe', 'Vana-Lõuna', 'Vana-Merivälja', 'Vana-Mustamäe', 'Vana-Posti',
        'Vana-Pärnu', 'Vana-Tartu', 'Vanaturu', 'Vana-Umboja', 'Vana-Veerenni', 'Vana-Viru', 'Vana', 'Vanemuise', 'Vaniku',
        'Vaprate', 'Varblase', 'Varbola', 'Varese', 'Varese', 'Varju', 'Varjulille', 'Varraku', 'Varre',
        'Varsaallika', 'Varsaallika', 'Vasara', 'Vase', 'Vati', 'Vedru', 'Veduri', 'Vee', 'Veerenni',
        'Veerenni', 'Veeriku', 'Veerise', 'Veerme', 'Veetorni', 'Vene', 'Versta', 'Vesikaare', 'Vesilennuki',
        'Vesioina', 'Vesipapi', 'Vesiravila', 'Vesiveski', 'Vesivärava', 'Veski', 'Veskilise', 'Veskimetsa', 'Veskimäe',
        'Veskiposti', 'Vesse', 'Vesse', 'Vete', 'Viadukti', 'Videviku', 'Vibu', 'Vigla', 'Vihitaja',
        'Vihu', 'Vihuri', 'Viige', 'Viimsi', 'Viimsi', 'Eduard', 'Viirpuu', 'Viisi', 'Vikerkaare',
        'Vikerlase', 'Eduard', 'Vile', 'Vilisuu', 'Viljandi', 'Villardi', 'Villkäpa', 'Villpea', 'Jüri',
        'Vilu', 'Vimma', 'Vindi', 'Vineeri', 'Vinkli', 'Virbi', 'Virmalise', 'Viru', 'Viru',
        'Virve', 'Visase', 'Vismeistri', 'Viu', 'Viu', 'Volta', 'Voo', 'Voolu', 'Voorimehe',
        'Vormsi', 'Vuti', 'Võidu', 'Võidu', 'Võidujooksu', 'Võistluse', 'Võlvi', 'Võra', 'Võrgu',
        'Võrgukivi', 'Võrse', 'Võrsiku', 'Võru', 'Võsa', 'Võsara', 'Võsu', 'Võtme', 'Vähi',
        'Väike', 'Väike', 'Väikese', 'Väike-Ameerika', 'Väike-Erika', 'Väike-Gonsiori', 'Väike-Karja', 'Väike-Karjamaa', 'Väike-Kloostri',
        'Väike-Kompassi', 'Väike-Laagri', 'Väike-Laevaehituse', 'Väike-Lasnamäe', 'Väike-Männiku', 'Väike-Paala', 'Väike-Patarei', 'Väike-Pärnu', 'Väike-Pääsukese',
        'Väike-Raevalla', 'Väike-Roosikrantsi', 'Väike-Sõjamäe', 'Väike-Söörensi', 'Väike-Tartu', 'Väina', 'Välgu', 'Välja', 'Väo',
        'Väomurru', 'Värava', 'Värsi', 'Värvi', 'Västriku', 'Vääna', 'Vööri', 'August',
        'Ferdinand', 'Wismari', 'Õhtu', 'Õie', 'Õie', 'Õilme', 'Õismäe', 'Õitse',
        'Õle', 'Õllepruuli', 'Õnne', 'Õpetajate', 'Õuna', 'Ädala', 'Äigrumäe', 'Äkke',
        'Ämariku', 'Ääre', 'Ääsi', 'Ööbiku', 'Öölase', 'Ülase', 'Ülemiste',
        'Ülem-Jõe', 'Ülem-Luha', 'Üliõpilaste', 'Ümera',
    ];

    protected static $addressFormats = [
        '{{postcode}}, {{city}} {{street}} {{buildingNumber}}',
    ];

    public static function buildingNumber()
    {
        return static::bothify(static::randomElement(static::$buildingNumber));
    }

    public function address()
    {
        $format = static::randomElement(static::$addressFormats);

        return $this->generator->parse($format);
    }

    public static function country()
    {
        return static::randomElement(static::$country);
    }

    public static function postcode()
    {
        return static::toUpper(static::bothify(static::randomElement(static::$postcode)));
    }

    public function city()
    {
        return static::randomElement(static::$city);
    }

    public static function street()
    {
        return static::randomElement(static::$street);
    }
}
