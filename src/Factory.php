<?php

declare(strict_types=1);

namespace Faker\Estonia;

use Faker\Generator;

class Factory extends \Faker\Factory
{
    public static function estonia(): Generator
    {
        $generator = new Generator();

        foreach (static::$defaultProviders as $provider) {
            $providerClassName = static::getProviderClassname($provider);
            $generator->addProvider(new $providerClassName($generator));
        }

        return $generator;
    }

    /**
     * {@inheritDoc}
     */
    protected static function getProviderClassname($provider, $locale = 'et_EE')
    {
        $providerClass = 'Faker\\Estonia\\' . $provider;

        if (class_exists($providerClass, true)) {
            return $providerClass;
        }

        return parent::getProviderClassname($provider, $locale);
    }
}
